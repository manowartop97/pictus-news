<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-08-20
 * Time: 12:24
 */

namespace Pictus\News\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\View\View;
use Pictus\News\Models\NewModel;

/**
 * Class NewsController
 * @package Pictus\News\Http\Controllers\Admin
 */
class NewsController extends Controller
{
    /**
     * Index view
     *
     * @return View
     */
    public function index(): View
    {
        return view('pictus.news.index');
    }

    /**
     * Form view
     *
     * @param NewModel|null $model
     * @return View
     */
    public function form(NewModel $model = null): View
    {
        return view('pictus.news.form', [
            'model' => $model,
            'files' => !is_null($model) ? $model->files()->with('file')->get()->all() : null,
            'title' => is_null($model) ? 'Новая новость' : 'Обновление новости'
        ]);
    }
}
