<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-08-18
 * Time: 15:39
 */

namespace Pictus\News\Http\Controllers\Api;

use App\Api\V1\Http\Controllers\ApiController;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Response;
use Pictus\News\Http\Requests\News\UpdateStatusRequest;
use Pictus\News\Http\Resources\NewsResource;
use Pictus\News\Http\Requests\News\CreateRequest;
use Pictus\News\Http\Requests\News\SearchRequest;
use Pictus\News\Http\Requests\News\UpdateRequest;
use Pictus\News\Models\NewModel;
use Pictus\News\Services\NewsService\Contracts\NewsServiceInterface;

/**
 * Class NewsController
 * @package Pictus\News\Http\Controllers
 */
class NewsController extends ApiController
{
    /**
     * @var NewsServiceInterface
     */
    private $newsService;

    /**
     * NewsController constructor.
     * @param NewsServiceInterface $newsService
     */
    public function __construct(NewsServiceInterface $newsService)
    {
        $this->newsService = $newsService;
    }

    /**
     * Get news
     *
     * @param SearchRequest $request
     * @return AnonymousResourceCollection
     */
    public function index(SearchRequest $request): AnonymousResourceCollection
    {
        return NewsResource::collection($this->newsService->getAll($request->validated()));
    }

    /**
     * @param CreateRequest $request
     * @return NewsResource
     * @throws Exception
     */
    public function create(CreateRequest $request): NewsResource
    {
        if (is_null($model = $this->newsService->create($request->validated()))) {
            throw new Exception('Не удалось создать новость', 400);
        }

        return new NewsResource($model);
    }

    /**
     * @param NewModel $model
     * @param UpdateRequest $request
     * @return NewsResource
     * @throws Exception
     */
    public function update(NewModel $model, UpdateRequest $request): NewsResource
    {
        if (is_null($model = $this->newsService->update($model, $request->validated()))) {
            throw new Exception('Не удалось обновить новость', 400);
        }

        return new NewsResource($model);
    }

    /**
     * @param NewModel $model
     * @return JsonResponse
     * @throws Exception
     */
    public function delete(NewModel $model): JsonResponse
    {
        if (!$this->newsService->delete($model)) {
            throw new Exception('Не удалось удалить новость', 400);
        }

        return Response::json(null, 200);
    }

    /**
     * Update model status
     *
     * @param NewModel $model
     * @param UpdateStatusRequest $request
     * @return NewsResource
     * @throws Exception
     */
    public function updateStatus(NewModel $model, UpdateStatusRequest $request): NewsResource
    {
        if (is_null($model = $this->newsService->update($model, $request->validated()))) {
            throw new Exception('Не удалось обновить статус новости', 400);
        }

        return new NewsResource($model);
    }

    /**
     * Copy new instance
     *
     * @param NewModel $model
     * @return NewsResource
     * @throws Exception
     */
    public function copy(NewModel $model): NewsResource
    {
        if (is_null($model = $this->newsService->copy($model))) {
            throw new Exception('Не удалось копировать новость', 400);
        }

        return new NewsResource($model);
    }
}
