<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-08-22
 * Time: 12:02
 */

namespace Pictus\News\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\View\View;
use Pictus\News\Models\NewModel;
use Pictus\News\Services\NewsService\Contracts\NewsServiceInterface;

/**
 * Class NewsController
 * @package Pictus\News\Http\Controllers\Client
 */
class NewsController extends Controller
{
    /**
     * @var NewsServiceInterface
     */
    private $newsService;

    /**
     * NewsController constructor.
     * @param NewsServiceInterface $newsService
     */
    public function __construct(NewsServiceInterface $newsService)
    {
        $this->newsService = $newsService;
    }

    /**
     * News list
     *
     * @return View
     */
    public function index(): View
    {
        return view('pictus.news.client.index', [
            'news' => $this->newsService->getAll(['status' => NewModel::STATUS_ACTIVE])
        ]);
    }

    /**
     * Show new
     *
     * @param string $slug
     * @return View
     * @throws Exception
     */
    public function show(string $slug): View
    {
        $newModel = NewModel::query()->where('slug', $slug)->get()->first();

        if (!$newModel) {
            throw new Exception("Новость не найдена");
        }

        if($newModel->status !== NewModel::STATUS_ACTIVE){
            throw new Exception("Новость не найдена");
        }

        return view('pictus.news.client.new-item', [
            'model' => $newModel
        ]);
    }

}
