<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-08-18
 * Time: 15:50
 */

namespace Pictus\News\Http\Requests\News;

use Pictus\News\Http\Requests\Request;
use Pictus\News\Models\NewModel;

/**
 * Class CreateRequest
 * @package Pictus\News\Http\Requests\News
 */
class CreateRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'title'        => 'required|string',
            'publish_date' => 'required|date_format:Y-m-d H:i',
            'content'      => 'required|string',
            'status'       => 'nullable|integer|in:' . NewModel::STATUS_ACTIVE . ',' . NewModel::STATUS_INACTIVE . ',' . NewModel::STATUS_MODERATION,
            'file_ids'     => 'nullable|array',
            'file_ids.*'   => 'required|integer|exists:files,id'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}
