<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-08-18
 * Time: 15:54
 */

namespace Pictus\News\Http\Requests\News;

use Pictus\News\Http\Requests\Request;

/**
 * Class SearchRequest
 * @package Pictus\News\Http\Requests\News
 */
class SearchRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'nullable|string'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}
