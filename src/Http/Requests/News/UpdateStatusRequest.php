<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-08-21
 * Time: 19:42
 */

namespace Pictus\News\Http\Requests\News;

use Pictus\News\Http\Requests\Request;
use Pictus\News\Models\NewModel;

/**
 * Class UpdateStatusRequest
 * @package Pictus\News\Http\Requests\News
 */
class UpdateStatusRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'status'       => 'nullable|integer|in:' . NewModel::STATUS_ACTIVE . ',' . NewModel::STATUS_INACTIVE . ',' . NewModel::STATUS_MODERATION,
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}
