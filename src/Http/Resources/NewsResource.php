<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-08-20
 * Time: 12:25
 */

namespace Pictus\News\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Pictus\News\Models\NewModel;

/**
 * Class NewsResource
 * @package Pictus\News\Http\Controllers\Resources
 *
 * @property NewModel $resource
 */
class NewsResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->resource->id,
            'title'                 => $this->resource->title,
            'content'               => $this->resource->content,
            'status'                => $this->resource->status,
            'publish_date'          => $this->resource->publish_date->format('d M Y'),
            'is_future_publication' => $this->resource->publish_date->timestamp > time(),
            'client_link'           => route('pictus.client.news.show', ['slug' => $this->resource->slug])
        ];
    }
}

