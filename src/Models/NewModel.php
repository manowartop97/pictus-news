<?php

namespace Pictus\News\Models;

use App\Models\FileManager\EntityFile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Carbon;

/**
 * @property integer $id
 * @property string $slug
 * @property string $title
 * @property Carbon $publish_date
 * @property string $content
 * @property integer $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property EntityFile[] $files
 */
class NewModel extends Model
{
    /**
     * @const
     */
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_MODERATION = 2;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'news';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['slug', 'title', 'publish_date', 'content', 'status', 'created_at', 'updated_at'];

    /**
     * @return MorphMany
     */
    public function files(): MorphMany
    {
        return $this->morphMany(EntityFile::class, 'attachable');
    }
}
