<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-08-18
 * Time: 16:39
 */

namespace Pictus\News\Observers;

use App\Models\FileManager\EntityFile;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Pictus\News\Models\NewModel;

/**
 * Class NewsObserver
 * @package Pictus\News\Observers
 */
class NewsObserver
{
    /**
     * @param NewModel $model
     * @return void
     */
    public function retrieved(NewModel $model): void
    {
        $model->publish_date = Carbon::create($model->publish_date);
    }

    /**
     * @param NewModel $model
     * @return void
     */
    public function creating(NewModel $model): void
    {
        $model->publish_date = Carbon::create($model->publish_date);
        $model->slug = $this->generateUniqueSlug($model->title);
    }

    /**
     * @param NewModel $model
     * @return void
     */
    public function updating(NewModel $model): void
    {
        if ($model->isDirty('title')) {
            $model->slug = $this->generateUniqueSlug($model->title);
        }

        if (!$model->publish_date instanceof Carbon) {
            $model->publish_date = Carbon::create($model->publish_date);
        }
    }

    /**
     * @param NewModel $model
     * @return void
     */
    public function deleting(NewModel $model): void
    {
        EntityFile::query()->where([['attachable_id', $model->id], ['attachable_type', get_class($model)]])->delete();
        $model->files()->delete();
    }

    /**
     * Generate unique slug for new
     *
     * @param string $title
     * @return string
     */
    protected function generateUniqueSlug(string $title): string
    {
        $counter = 1;

        $slug = Str::slug($title);

        while (NewModel::query()->where('slug', $slug)->exists()) {
            $slug = Str::slug($title . '-' . $counter);
            $counter++;
        }

        return $slug;
    }
}
