<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-08-18
 * Time: 14:41
 */

namespace Pictus\News\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Pictus\News\Models\NewModel;
use Pictus\News\Observers\NewsObserver;
use Pictus\News\Repositories\NewsRepository\Contracts\NewsRepositoryInterface;
use Pictus\News\Repositories\NewsRepository\NewsRepository;
use Pictus\News\Services\NewsService\Contracts\NewsServiceInterface;
use Pictus\News\Services\NewsService\NewsService;

/**
 * Class NewsServiceProvider
 * @package App\packages\pictus\news\src\Providers
 */
class NewsServiceProvider extends ServiceProvider
{
    /**
     * Register the application services
     *
     * @return void
     */
    public function register(): void
    {

    }

    /**
     * Bootstrap the application services
     *
     * @param Router $router
     * @return void
     */
    public function boot(Router $router): void
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/api.php');
        $this->loadRoutesFrom(__DIR__ . '/../routes/admin.php');
        $this->loadRoutesFrom(__DIR__ . '/../routes/client.php');
        $this->loadMigrationsFrom(__DIR__ . '/../migrations');
        $this->loadViewsFrom(__DIR__.'/../views', 'news');
        $this->publishes([__DIR__.'/../views' => base_path('resources/views/pictus/news'),]);
        $this->publishes([__DIR__ . '/../resources/assets' => resource_path('assets/vendor/news')], 'vue-components');

        $this->app->singleton(NewsServiceInterface::class, NewsService::class);
        $this->app->singleton(NewsRepositoryInterface::class, NewsRepository::class);

        NewModel::observe(NewsObserver::class);
    }
}
