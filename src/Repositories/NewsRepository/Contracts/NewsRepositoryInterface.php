<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-08-18
 * Time: 15:43
 */

namespace Pictus\News\Repositories\NewsRepository\Contracts;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Pictus\News\Models\NewModel;

/**
 * Interface NewsRepositoryInterface
 * @package Pictus\News\Repositories\NewsRepository\Contracts
 */
interface NewsRepositoryInterface
{
    /**
     * @param array $search
     * @return Collection
     */
    public function getFilteredCollection(array $search = []): Collection;

    /**
     * @param array $search
     * @param int $pageSize
     * @return LengthAwarePaginator
     */
    public function getFilteredAndPaginatedCollection(array $search = [], int $pageSize = 15): LengthAwarePaginator;

    /**
     * Create model
     *
     * @param array $data
     * @return NewModel|null
     */
    public function create(array $data): ?NewModel;

    /**
     * Update model
     *
     * @param NewModel $model
     * @param array $data
     * @return NewModel|null
     */
    public function update(NewModel $model, array $data): ?NewModel;

    /**
     * Delete model
     *
     * @param NewModel $model
     * @return bool
     */
    public function delete(NewModel $model): bool;
}
