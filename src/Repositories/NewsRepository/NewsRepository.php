<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-08-18
 * Time: 15:45
 */

namespace Pictus\News\Repositories\NewsRepository;

use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Pictus\News\Models\NewModel;
use Pictus\News\Repositories\NewsRepository\Contracts\NewsRepositoryInterface;

/**
 * Class NewsRepository
 * @package Pictus\News\Repositories\NewsRepository
 */
class NewsRepository implements NewsRepositoryInterface
{

    /**
     * @param array $search
     * @return Collection
     */
    public function getFilteredCollection(array $search = []): Collection
    {
        return $this->getFilteredQuery($search)->get();
    }

    /**
     * @param array $search
     * @param int $pageSize
     * @return LengthAwarePaginator
     */
    public function getFilteredAndPaginatedCollection(array $search = [], int $pageSize = 15): LengthAwarePaginator
    {
        return $this->getFilteredQuery($search)->paginate($pageSize);
    }

    /**
     * Create model
     *
     * @param array $data
     * @return NewModel|null
     */
    public function create(array $data): ?NewModel
    {
        $model = new NewModel($data);

        if (!$model->save()) {
            return null;
        }

        return $model;
    }

    /**
     * Update model
     *
     * @param NewModel $model
     * @param array $data
     * @return NewModel|null
     */
    public function update(NewModel $model, array $data): ?NewModel
    {
        if (!$model->fill($data)->save()) {
            return null;
        }

        return $model;
    }

    /**
     * Delete model
     *
     * @param NewModel $model
     * @return bool
     * @throws Exception
     */
    public function delete(NewModel $model): bool
    {
        return !is_null($model->delete());
    }

    /**
     * @param array $search
     * @return Builder
     */
    protected function getFilteredQuery(array $search): Builder
    {
        return NewModel::query()
            ->when(isset($search['title']), function (Builder $query) use ($search) {
                $query->where('title', 'like', "%{$search['title']}%");
            })
            ->when(isset($search['status']), function (Builder $query) use ($search) {
                $query->where('status', $search['status']);
            })
            ->orderBy('id', 'desc');
    }
}
