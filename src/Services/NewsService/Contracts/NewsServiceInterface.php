<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-08-18
 * Time: 15:40
 */

namespace Pictus\News\Services\NewsService\Contracts;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Pictus\News\Models\NewModel;

/**
 * Interface NewsServiceInterface
 * @package Pictus\News\Services\NewsService\Contracts
 */
interface NewsServiceInterface
{
    /**
     * Get filtered and paginated models
     *
     * @param array $search
     * @param int $pageSize
     * @return LengthAwarePaginator
     */
    public function getAll(array $search = [], int $pageSize = 15): LengthAwarePaginator;

    /**
     * Create model
     *
     * @param array $data
     * @return NewModel|null
     */
    public function create(array $data): ?NewModel;

    /**
     * Update model
     *
     * @param NewModel $model
     * @param array $data
     * @return NewModel|null
     */
    public function update(NewModel $model, array $data): ?NewModel;

    /**
     * Delete model
     *
     * @param NewModel $model
     * @return bool
     */
    public function delete(NewModel $model): bool;

    /**
     * Copy model
     *
     * @param NewModel $model
     * @return Model|null
     */
    public function copy(NewModel $model): ?Model;
}
