<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 2019-08-18
 * Time: 15:42
 */

namespace Pictus\News\Services\NewsService;

use App\Models\FileManager\EntityFile;
use App\Models\FileManager\File;
use DB;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Pictus\News\Models\NewModel;
use Pictus\News\Repositories\NewsRepository\Contracts\NewsRepositoryInterface;
use Pictus\News\Services\NewsService\Contracts\NewsServiceInterface;

/**
 * Class NewsService
 * @package Pictus\News\Services\NewsService
 */
class NewsService implements NewsServiceInterface
{
    /**
     * @var NewsRepositoryInterface
     */
    protected $newsRepository;

    /**
     * NewsService constructor.
     * @param NewsRepositoryInterface $newsRepository
     */
    public function __construct(NewsRepositoryInterface $newsRepository)
    {
        $this->newsRepository = $newsRepository;
    }

    /**
     * Get filtered and paginated models
     *
     * @param array $search
     * @param int $pageSize
     * @return LengthAwarePaginator
     */
    public function getAll(array $search = [], int $pageSize = 15): LengthAwarePaginator
    {
        return $this->newsRepository->getFilteredAndPaginatedCollection($search, $pageSize);
    }

    /**
     * Create model
     *
     * @param array $data
     * @return NewModel|null
     * @throws Exception
     */
    public function create(array $data): ?NewModel
    {
        DB::beginTransaction();
        $model = $this->newsRepository->create($data);

        if (is_null($model)) {
            DB::rollBack();
            return null;
        }

        if (isset($data['file_ids']) && !$this->attachFiles($model, $data['file_ids'])) {
            DB::rollBack();
            return null;
        }

        DB::commit();
        return $model;
    }

    /**
     * Update model
     *
     * @param NewModel $model
     * @param array $data
     * @return NewModel|null
     * @throws Exception
     */
    public function update(NewModel $model, array $data): ?NewModel
    {
        DB::beginTransaction();
        $model = $this->newsRepository->update($model, $data);

        if (is_null($model)) {
            DB::rollBack();
            return null;
        }

        $currentFileIds = $model->files()->pluck('file_id', 'file_id')->toArray();
        if (isset($data['file_ids']) && !empty(array_diff($data['file_ids'], $currentFileIds))) {
            $model->files()->delete();
            if (!$this->attachFiles($model, $data['file_ids'])) {
                DB::rollBack();
                return null;
            }
        }

        DB::commit();
        return $model;
    }

    /**
     * Delete model
     *
     * @param NewModel $model
     * @return bool
     */
    public function delete(NewModel $model): bool
    {
        return $this->newsRepository->delete($model);
    }

    /**
     * Copy model
     *
     * @param NewModel $model
     * @return Model|null
     * @throws Exception
     */
    public function copy(NewModel $model): ?Model
    {
        $data = array_merge(
            $model->toArray(),
            ['file_ids' => $model->files()->select('file_id')->pluck('file_id')->toArray()]
        );

        unset($data['created_at'], $data['updated_at']);
        $data['publish_date'] = $model->publish_date->format('Y-m-d H:i');

        return $this->create($data);
    }

    /**
     * Attach files to new
     *
     * @param Model|NewModel $model
     * @param array $fileIds
     * @return bool
     */
    protected function attachFiles(Model $model, array $fileIds): bool
    {
        foreach ($fileIds as $fileId) {
            /** @var File $file */
            $file = File::query()->findOrFail($fileId);
            $result = $model->files()->create([
                'file_id'         => $fileId,
                'attachable_id'   => $model->id,
                'attachable_type' => get_class($model),
                'path'            => $file->path
            ]);

            if (!$result instanceof EntityFile) {
                return false;
            }
        }

        return true;
    }

}
