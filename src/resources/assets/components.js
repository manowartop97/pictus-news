import Vue from 'vue';

Vue.component('news-index', require('./components/NewsIndexComponent').default);
Vue.component('news-form', require('./components/NewsFormComponent').default);
