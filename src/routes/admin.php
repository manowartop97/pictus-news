<?php

Route::group(['prefix' => 'admin/news', 'middleware' => ['web', 'auth'], 'namespace' => 'Pictus\News\Http\Controllers'], function () {
    Route::get('', 'Admin\NewsController@index')->name('pictus.news.index');
    Route::get('/form/{model?}', 'Admin\NewsController@form')->name('pictus.news.form');
});
