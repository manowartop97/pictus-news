<?php
Route::group(['prefix' => 'api/v1', 'namespace' => 'Pictus\News\Http\Controllers', 'middleware' => ['web', 'auth']], function () {
    Route::group(['prefix' => 'news', 'namespace' => 'Api'], function () {
        Route::get('/', 'NewsController@index');
        Route::post('', 'NewsController@create');
        Route::put('/{model}', 'NewsController@update');
        Route::delete('/{model}', 'NewsController@delete');
        Route::patch('/{model}', 'NewsController@updateStatus');
        Route::post('/{model}/copy', 'NewsController@copy');
    });
});
