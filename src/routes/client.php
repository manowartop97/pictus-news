<?php

Route::group(['prefix' => 'novosti', 'middleware' => ['web'], 'namespace' => 'Pictus\News\Http\Controllers'], function () {
    Route::get('', 'Client\NewsController@index')->name('pictus.client.news.index');
    Route::get('/{slug}', 'Client\NewsController@show')->name('pictus.client.news.show');
});

