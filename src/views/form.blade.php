@extends('admin.layouts.main')
@section('title'){{$title}}@endsection
@section('content')
    @if(is_null($model))
        <news-form title="{{$title}}" create-route="{{route('pictus.news.form')}}"
                   index-route="{{route('pictus.news.index')}}"></news-form>
    @else
        <news-form new-model="{{json_encode($model)}}" create-route="{{route('pictus.news.form')}}"
                   files="{{json_encode($files)}}" title="{{$title}}"
                   index-route="{{route('pictus.news.index')}}"></news-form>
    @endif
@endsection
